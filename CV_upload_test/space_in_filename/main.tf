resource "null_resource" "granular" {
  triggers = {
    trigger = "${var.false_string}"
  }
}

variable "false_string" {

}

output "false_string_output" {
  value = var.false_string
}
