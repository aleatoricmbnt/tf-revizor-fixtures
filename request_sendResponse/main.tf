terraform {
  required_providers {
    http = {
      source = "hashicorp/http"
      version = "2.1.0"
    }
  }
}

variable "baseUrl" {
  sensitive = false
}

variable "bearerToken" {
  sensitive = true
}

variable "requestUrl" {
  sensitive = false
}

variable "endpointUrl" {
  sensitive = false
}

data "http" "request" {
  url = "${var.baseUrl}/${var.requestUrl}"

  # Optional
  request_headers = {
    Accept = "application/json"
    Authorization = "Bearer ${var.bearerToken}"
    Prefer = "profile=internal"
  }

}

resource "null_resource" "sendJsonBody" {
  triggers = {
    trigger = "${var.requestUrl}"
  }
  provisioner "local-exec" {
    command = "curl -X POST -H 'Content-type: application/json' --data '${data.http.request.body}' ${var.endpointUrl}"
  }
}

output "response" {
  value = data.http.request.body
}

output "response_headers" {
  value = data.http.request.response_headers
}