terraform {
  required_providers {
    http = {
      source = "hashicorp/http"
      version = "2.1.0"
    }
  }
}

variable "base_url" {
  sensitive = false
  description = "Base URL for HTTP request. For example, https://aleatoric.main.scalr.dev/"
}

variable "bearer_token" {
  sensitive = true
  description = "Bearer token used for auth (for Scalr is generated in Scalr menu > API access)"
}

variable "request_url" {
  sensitive = false
  desctiption = "API endpoint for request"
}

data "http" "request" {
  url = "${var.base_url}/${var.request_url}"

  # Optional
  request_headers = {
    Accept = "application/json"
    Authorization = "Bearer ${var.bearer_token}"
    Prefer = "profile=internal"
  }

}


output "response_headers" {
  value = data.http.request.response_headers
}

output "response" {
  value = data.http.request.body
}