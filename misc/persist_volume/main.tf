terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
}


variable "run_id" {
  default = "run_id_1"
  sensitive = true
}

variable "sleep_time" {
  default = 35
}

data "archive_file" "init" {
  type        = "zip"
  source_file = "${path.module}/file.txt"
  output_path = "./output/archive.zip"
}


resource "aws_lambda_function" "ec2_termination_events" {
  function_name    = "core-ops-ec2-termination-events"
  source_code_hash = data.archive_file.init.output_base64sha256
  role             = ""
  filename         = data.archive_file.init.output_path
  handler          = "ec2-termination-events.lambda_handler"
  runtime          = "python3.8"
  timeout          = "300"

  tags = {
    owner = "oleg"
  }
}

module "web_server_sg" {
  source = "terraform-aws-modules/security-group/aws//modules/http-80"
  create = false
  name = "web-server"
  vpc_id = "vpc-12345678"
}
