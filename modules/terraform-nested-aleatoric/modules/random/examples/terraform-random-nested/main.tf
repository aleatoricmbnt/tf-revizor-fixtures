variable "trigger" {
  description = "The trigger value for the `random_integer` resource in this module."
  default     = "one"
}

resource "random_integer" "resource" {
  min = 0
  max = 1

  keepers = {
    number = "${var.trigger}"
  }
}

output "random_integer_id" {
  description = "The `id` of the `random_integer` resource in this module."
  value       = "${random_integer.resource.id}"
}