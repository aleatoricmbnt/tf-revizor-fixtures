resource "random_id" "test" {
  keepers = {
    timestamp = timestamp()
  }
}

output "generated_id" {
  value = random_id.test.id
}

