package terraform

import input.tfplan as tfplan

deny[reason] {
    number := to_number(tfplan.variables.food.value)
    required_cans := 3
    number < required_cans

    reason := sprintf("You have %d cans of food for your pet! You need to have at least %d to proceed, so your pet won't be hungry",[number,required_cans])
}