version = "v1"

policy "food_hard" {
  enabled           = true
  enforcement_level = "hard-mandatory"
}

policy "food_soft" {
  enabled           = true
  enforcement_level = "soft-mandatory"
}

policy "food_adv" {
  enabled           = true
  enforcement_level = "advisory"
}
