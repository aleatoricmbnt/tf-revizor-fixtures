version = "v1"

policy "xss_vars" {
  enabled           = true
  enforcement_level = "soft-mandatory"
}