package terraform

import input.tfplan as tfplan

deny[reason] {
    true

    reason := sprintf("Variables: %v",[tfplan.variables])
}