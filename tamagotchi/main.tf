resource "random_pet" "pet" {
  length = var.name_length
  separator = "👾"

  keepers = {
    pet_keeper = "${var.pet_keeper}"
  }
}

resource "random_integer" "age" {
  min = 1
  max = 2
  
  keepers = {
    pet_keeper = "${var.pet_keeper}"
  }
}
