variable "pet_keeper" {
  sensitive = false
  description = "Change variable value to generate a new pet (requires tf run)"
}

variable "name_length" {
 default = 2
  description = "Number of words in a pet's name"
}

variable "food" {
  description = "Number of cans of food stored for the pet"
} 
